resource "azurerm_resource_group" "oe-tf-rg-team1" {
  name     = "OE-DevOps-AKS-RG-TEAM1"
  location = "West Europe"
}

resource "azurerm_kubernetes_cluster" "oe-devops-aks-team1" {
  name                = "oe-devops-aks1-team1"
  location            = azurerm_resource_group.oe-tf-rg-team1.location
  resource_group_name = azurerm_resource_group.oe-tf-rg-team1.name
  dns_prefix          = "oedevopsaks1team1"

  identity {
    type = "SystemAssigned"
  }

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_A2_v2"
  }
}

resource "azurerm_container_registry" "oe-devops-acr-team1" {
  name                = "oedevopsacrteam1"
  resource_group_name = azurerm_resource_group.oe-tf-rg-team1.name
  location            = azurerm_resource_group.oe-tf-rg-team1.location
  sku                 = "Standard"
  admin_enabled       = true
}


#resource "azurerm_role_assignment" "oe-devops-role" {
#  principal_id                     = azurerm_kubernetes_cluster.oe-devops-aks.kubelet_identity[0].object_id
#  role_definition_name             = "AcrPull"
#  scope                            = azurerm_container_registry.oe-devops-acr.id
#  #principal_id                     = data.azuread_service_principal.aks_principal.id
#  skip_service_principal_aad_check = true
#}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.oe-devops-aks-team1.kube_config.0.client_certificate
}

output "kube_config" {
  value = nonsensitive(azurerm_kubernetes_cluster.oe-devops-aks-team1.kube_config_raw)
}

output "admin_password" {
  value       = nonsensitive(azurerm_container_registry.oe-devops-acr-team1.admin_password)
  description = "The object ID of the user"
}
